# Chevereto

`Docker`下`Chevereto`搭建环境，使用`php:7.2-fpm-alpine`镜像创建，自动安装`Nginx`及相关`PHP`模块，并使用`Supervisor`进行进程守护。

项目搭建`Chevereto`版本为目前最新的`1.1.0`版本，自动从`Chevereto`官方仓库下载tar.gz包。

需挂载存储卷，并链接至容器`/data/images`目录，用以存储网站所有数据。

# 使用方法

```bash
docker run -d --name chevereto -p 80:80 -v /your_volume_path/chevereto:/data/images ylqjgm/chevereto
```