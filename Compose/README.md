# Compose

这是一个多容器搭建脚本，自动搭建`nginx`、`letsencrypt`、`mysql`、`phpmyadmin`、`oneindex`、`chevereto`、`poste`、`mtproxy`、`shadowsocks`，容器搭建后自动绑定域名，并为绑定的域名申请`ssl`证书。

请修改脚本中的对应数据为您自己的数据。

# 使用方法

```bash
docker-compose up -d
```