# MTProxy

`Docker`下`MTProxy`代理环境，将使用`Alpine`镜像创建，并自动获取[ylqjgm/MTProxy](https://github.com/ylqjgm/MTProxy)项目代码进行编译。

代理需要配合`volumes`使用，用以存储程序自动创建的`Secret`，若不存在，则每次重启都会变更`Secret`。

# 使用方法

```bash
docker run -d --name mtproxy -p 8822:8822 -v /your_volume_path/mtproxy:/data ylqjgm/mtproxy
```